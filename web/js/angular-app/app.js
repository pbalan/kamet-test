'use strict';

/**
 * App level module with its dependencies,
 * and app level constants
 */
var app = angular.module('APP', [
    'ngRoute',
    'ui.bootstrap',
    'ngAnimate'
]);

app.config(['$qProvider', '$httpProvider', function ($qProvider, $httpProvider) {
    $qProvider.errorOnUnhandledRejections(true);
}]);

app.directive('user', [function () {
    return {
        restrict: 'E',
        transclude: true,
        replace: true,
        priority: -1000,
        scope: {
            actionUrl: "@",
            matches: "="
        },
        templateUrl: 'js/angular-app/user/partials/list.html'
    };
}]);

app.run(function($rootScope) {
    $rootScope.reload = $rootScope.$broadcast;
});
