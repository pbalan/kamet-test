'use strict';

/**
 *  === Controllers ===
 *
 *  User Controller,
 *  will fetch the users
 */
angular.module('APP').
controller('UserController', ['$scope', '$http', 'SharedService',
    function ($scope, $http, SharedService) {
        $scope.showPost = false;
        $scope.showPostDetail = false;

        $scope.users = [];
        $scope.user = {};
        $scope.posts = [];
        $scope.post = {};

        var getUsers = function () {
            $http.get(SharedService.getUsersRoute()).then(function (response) {
                if (response.data.length > 0) {
                    $scope.users = angular.copy(response.data);
                }
            });
        };

        var getPosts = function (user) {
            $http.get(SharedService.getUserPostsRoute(user.id)).then(function (response) {
                if (response.data.length > 0) {
                    $scope.user = user;
                    $scope.posts = angular.copy(response.data);
                }
            });
        };

        var getComments = function (post) {
            $http.get(SharedService.getPostCommentsRoute(post.id)).then(function (response) {
                if (response.data.length > 0) {
                    $scope.post = post;
                    $scope.comments = angular.copy(response.data);
                }
            });
        };

        $scope.showPostDetails = function (post) {
            $scope.showPost = false;
            $scope.showPostDetail = true;
            getComments(post);
        };

        $scope.showPosts = function (user) {
            $scope.showPost = !$scope.showPost;
            $scope.showPostDetail = false;
            getPosts(user);
        };

        $scope.showUser = function () {
            $scope.showPost = false;
            $scope.showPostDetail = false;
        };

        $scope.init = function () {
            getUsers();
        };

        $scope.init();
    }]);