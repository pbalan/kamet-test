'use strict';

/**
 *  === Controllers ===
 *
 *  Main Controller,
 *  will fetch the user
 */
angular.module('APP').
controller('MainController', ['$scope', '$location',
    function ($scope, $location) {

        /**
         * Update page title when route changes
         */
        $scope.$on('$routeChangeSuccess', function () {
            var path       = $location.path(),
                pathPieces = path.split('/'),
                pageTitle = '';

            $scope.routeChanging = false;
            switch (pathPieces[1]) {
                case 'users': pageTitle = '';
                    break;
            }
            $scope.pageTitle = pageTitle;
        });

        $scope.$on('$routeChangeStart', function () {
            $scope.routeChanging = true;
        });

        $scope.init = function () {
        };

        $scope.init();

    }]);