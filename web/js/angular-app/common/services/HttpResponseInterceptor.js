angular.module('APP').
    factory('HttpResponseInterceptor', ['$q', '$rootScope', function ($q, $rootScope) {
        return {
            'request': function (config) {
                $rootScope.elementDisabled = true;
                $rootScope.isLoading = true;
                return config;
            },
            'response': function (response) {
                var errorMsg = '', errors = [];
                $rootScope.elementDisabled = false;
                $rootScope.isLoading = false;
                // handle caught errors gracefully
                if (response.data.status && response.data.status === 'error') {

                    // apple connect error or any other generic error
                    if (response.data.errorCode && response.data.errorCode === 403) {

                        errorMsg = response.data.error;

                    } else if (response.data.errors && response.data.errors.length) {

                        // list of error messages
                        errorMsg = response.data.msg;
                        errors   = response.data.errors;

                    } else {

                        // single error message
                        errorMsg = response.data.msg;
                    }
                    if (response.data.modal == undefined) {
                        return $q.reject(response);
                    } else {
                        return response;
                    }
                }

                return response;
            },

            'responseError': function (response) {

                var errorMsg = '',
                    status   = response.status;

                if (status === 500) {
                    errorMsg = 'Service unavailable. Error code: ' + status;
                } else if (status === 404) {
                    errorMsg = 'Not found. Error code: ' + status;
                } else if (status === 403) {
                    errorMsg = 'Access Denied. Error code: ' + status;
                } else {
                    errorMsg = 'Error occurred. Error code: ' + status;
                }

                // error handler for any unexpected errors
                return $q.reject(response);
            }
        };
}]);
