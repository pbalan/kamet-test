'use strict';

/**
 * === Services ===
 *
 * Shared Service will house the necessary properties and
 * functions which will be shared between controllers
 */

angular.module('APP').
factory('SharedService', [function () {

    var sharedService = {
        apiHost: 'http://jsonplaceholder.typicode.com',
        getUsersRoute: function () {
            return sharedService.apiHost + '/users';
        },
        getUserPostsRoute: function (userId) {
            return sharedService.apiHost + '/users/' + userId+ '/posts'
        },
        getPostCommentsRoute: function (postId) {
            return sharedService.apiHost + '/posts/' + postId + '/comments'
        }
    };
    return sharedService;
}]);
