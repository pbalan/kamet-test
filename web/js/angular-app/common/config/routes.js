'use strict';

/**
 * === Config ===
 *
 * Define application routes
 * and link them with controllers and templates
 */
angular.module('APP').
config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {

  // add handy headers to Ajax requests so that can
  // be verified on server side to detect Ajax request
    $httpProvider.defaults.useXDomain = true;

  // add custom response interceptor,
  // so all the responses will go through here,
  // this is the recommended Angular way to globally handle Ajax errors
  $httpProvider.interceptors.push('HttpResponseInterceptor');
    //delete $httpProvider.defaults.headers.common['X-Requested-With'];

  // register routes
  $routeProvider.
  when('/users', {
    templateUrl: 'js/angular-app/users/partials/list.html',
    controller: 'UserController'
  }).
  otherwise({redirectTo: '/users'});
}]);
